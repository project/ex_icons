CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Deep look
  * Icons as Plugins
  * Theme element
  * Twig function
  * Field API


INTRODUCTION
------------

This module creates a bridge between other modules or themes in integrating
[external-referencing SVG icons] defined in sprite sheets into Drupal.

This module provides a visual UI for end users to be able to select icons
provided by extensions. This module provides no icons by itself.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.


DEEP LOOK
---------

### Icons as Plugins

Icons are automatically discovered from the icon sheet at
`module_or_theme/dist/icons.svg` and are recorded in Drupal as plugins. A
simple example of a file:

```xml
<svg>
  <!--
    Recommended to store symbol elements in the defs tag as it is designed to
    store graphical objects that will be used at a later time.
  -->
  <defs>
    <!--
      The `viewBox` attribute should be included on symbols — this module uses
      the attribute to determine the aspect ratio of icons it discovers.
    -->
    <symbol id="triangle" viewBox="0 0 20 20">
      <path d="M0 20l10-20l10 20z"/>
    </symbol>
  </defs>

  <!--
    However this module does not make any considerations about where symbol
    elements are placed (for maximum compatibility with build processes).
  -->
  <symbol id="circle" viewBox="0 0 30 30">
    <circle cx="15" cy="15" r="15"/>
  </symbol>
</svg>
```

There is a convenience [Drush] command to clear the definitions cache for these:

```sh
$ drush cache-clear ex-icons
```

### Theme element

A theme function called `ex_icon` can be used as the `#theme` key to render any
icon from a sprite sheet. Use `#id` key to specify the icon.

```php
$render = [
  '#theme' => 'ex_icon',
  '#id' => 'arrow',
  '#attributes' => [
    'title' => t('Show more'),
    'width' => 25,
  ],
];
```

If a `title` attribute is set, then the SVG element will be given a `role` of
`img`, otherwise it will be `presentation`. If only one dimension attribute is
set **and** is a unitless number, then the other will be calculated
automatically from the source icon's `viewBox` attribute. This is to size the
SVG more closely to its content, otherwise the browser will default the SVG's
dimensions to 300 × 150 pixels.

### Twig function

Similar format as the theme element for use inline in twig templates. First
argument is the icon ID and the second is a hash (or attributes object) of
any attributes to add to the SVG (optional).

```twig
{{ ex_icon('shopping-cart', { height: 20 }) }}
```

### Form element

An icon selection form element to allow picking of an icon graphically.

```php
$form['icon'] = [
  '#type' => 'ex_icon_select',
  '#title' => $this->t('Accompanying icon'),
  '#default_value' => $this->getSetting('icon'),
];
```

### Field API integration

* Field type: Icon — Stores an icon's ID as a string value, with an optional
  text alternative input.
* Field widget: Icon select — Uses the icon form select element to select icon
  value(s).
* Field formatter: Icon — Display an icon. Works with icon as well as core's
  string field types.
* Field formatter: Link as icon — Display a *link* field as an icon instead of
  text.


[external-referencing SVG icons]:
https://css-tricks.com/svg-use-with-external-reference-take-2/
[Drush]: https://www.drush.org/
