<?php

namespace Drupal\ex_icons;

use Drupal\Component\Plugin\Discovery\CachedDiscoveryInterface;
use Drupal\Component\Plugin\PluginManagerInterface;

/**
 * Defines an interface for SVG external-use icon plugin managers.
 */
interface ExIconsManagerInterface extends PluginManagerInterface, CachedDiscoveryInterface {

  /**
   * Returns an array of possible values with labels for display.
   *
   * @return string[]
   *   An array of possible icon options that are currently known. The array
   *   returned is a flat array of option labels keyed by values.
   */
  public function getIconOptions();

}
