<?php

namespace Drupal\ex_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;
use Drupal\Core\Utility\Token;
use Drupal\link\LinkItemInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'ex_icon_link' formatter.
 *
 * @FieldFormatter(
 *   id = "ex_icon_link",
 *   label = @Translation("Link as icon"),
 *   field_types = {
 *     "link",
 *   },
 * )
 */
class ExIconLinkFormatter extends ExIconDefaultFormatter implements ContainerFactoryPluginInterface {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('token')
    );
  }

  /**
   * Constructs a new ExIconLinkFormatter.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Third party settings.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, Token $token) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->token = $token;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'icon'   => '',
      'rel'    => '',
      'target' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['icon'] = [
      '#title' => $this->t('Icon'),
      '#type' => 'ex_icon_select',
      '#default_value' => $this->getSetting('icon'),
      '#description' => $this->t('The icon to use.'),
      '#required' => TRUE,
    ];

    $elements['rel'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add rel="nofollow" to links'),
      '#return_value' => 'nofollow',
      '#default_value' => $this->getSetting('rel'),
    ];

    $elements['target'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Open link in new window'),
      '#return_value' => '_blank',
      '#default_value' => $this->getSetting('target'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();

    $summary   = parent::settingsSummary();
    $summary[] = $this->t('Icon: @icon', ['@icon' => $settings['icon']]);

    if (!empty($settings['rel'])) {
      $summary[] = $this->t('Add rel="@rel"', ['@rel' => $settings['rel']]);
    }

    if (!empty($settings['target'])) {
      $summary[] = $this->t('Open link in new window');
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    $entity        = $items->getEntity();
    $token_data    = [$entity->getEntityTypeId() => $entity];
    $token_options = ['clear' => TRUE];

    foreach ($items as $delta => $item) {
      $url   = $this->buildUrl($item);
      $title = empty($item->title)
        ? $this->fieldDefinition->getLabel()
        : $this->token->replace($item->title, $token_data, $token_options);

      $element[$delta] = [
        '#type' => 'link',
        '#url' => $url,
        '#options' => $url->getOptions(),
        '#title' => [
          '#theme' => 'ex_icon',
          '#id' => $this->getSetting('icon'),
          '#title'  => $title,
          // Use array_filter() to remove height or width if they are not set so
          // the dimension can be calculated in template_preprocess_ex_icon().
          '#attributes' => array_filter([
            'width'  => $this->getSetting('width'),
            'height' => $this->getSetting('height'),
          ]),
        ],
      ];
    }

    return $element;
  }

  /**
   * Builds the \Drupal\Core\Url object for a link field item.
   *
   * @param \Drupal\link\LinkItemInterface $item
   *   The link field item being rendered.
   *
   * @return \Drupal\Core\Url
   *   A Url object.
   */
  protected function buildUrl(LinkItemInterface $item) {
    $url = $item->getUrl() ?: Url::fromRoute('<none>');

    $settings = $this->getSettings();
    $options  = $item->options + $url->getOptions();

    // Add optional 'rel' attribute to link options.
    if (!empty($settings['rel'])) {
      $options['attributes']['rel'] = $settings['rel'];
    }
    // Add optional 'target' attribute to link options.
    if (!empty($settings['target'])) {
      $options['attributes']['target'] = $settings['target'];
    }

    if (!empty($item->_attributes)) {
      $options += ['attributes' => []];
      $options['attributes'] += $item->_attributes;
      // Unset field item attributes since they have been included in the
      // formatter output and should not be rendered in the field template.
      unset($item->_attributes);
    }

    $url->setOptions($options);

    return $url;
  }

}
