<?php

namespace Drupal\ex_icons\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'ex_icon_default' formatter.
 *
 * @FieldFormatter(
 *   id = "ex_icon_default",
 *   label = @Translation("Icon"),
 *   field_types = {
 *     "ex_icon",
 *     "list_string",
 *     "string",
 *   },
 * )
 */
class ExIconDefaultFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'height' => '',
      'width' => '',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements['width'] = [
      '#title' => $this->t('Width'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('width'),
      '#size' => 10,
    ];

    $elements['height'] = [
      '#title' => $this->t('Height'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('height'),
      '#size' => 10,
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    $settings = $this->getSettings();

    if (!empty($settings['width'])) {
      $summary[] = $this->t('Width: @width', ['@width' => $settings['width']]);
    }

    if (!empty($settings['height'])) {
      $summary[] = $this->t('Height: @height', ['@height' => $settings['height']]);
    }

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#theme' => 'ex_icon',
        '#id' => $item->value,
        '#title' => $item->title,
        // Use array_filter() to remove height or width if they are not set so
        // the dimension can be calculated in template_preprocess_ex_icon().
        '#attributes' => array_filter([
          'width'  => $this->getSetting('width'),
          'height' => $this->getSetting('height'),
        ]),
      ];
    }

    return $elements;
  }

}
