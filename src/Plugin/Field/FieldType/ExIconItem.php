<?php

namespace Drupal\ex_icons\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\StringItemBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'ex_icon' field type.
 *
 * @FieldType(
 *   id = "ex_icon",
 *   label = @Translation("Icon"),
 *   description = @Translation("This field stores an icon choice."),
 *   category = "general",
 *   default_widget = "ex_icon_select",
 *   default_formatter = "ex_icon_default",
 * )
 */
class ExIconItem extends StringItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings() {
    return [
      'title' => DRUPAL_OPTIONAL,
    ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Icon ID'))
      ->addConstraint('Length', ['max' => 127])
      ->setRequired(TRUE);

    $properties['title'] = DataDefinition::create('string')
      ->setLabel(t('Text alternative'))
      ->addConstraint('Length', ['max' => 255]);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => 'Icon ID.',
          'type' => 'varchar',
          'length' => 127,
        ],
        'title' => [
          'description' => 'Text alternative of the icon.',
          'type' => 'varchar',
          'length' => 255,
        ],
      ],
      'indexes' => [
        'value' => [['value', 20]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return 'value';
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $options = \Drupal::service('ex_icons.manager')->getIconOptions();
    $settings = $field_definition->getSettings();

    $values['value'] = array_rand(array_keys($options));
    $values['title'] = $settings['title'] ? (new Random())->word(mt_rand(1, 255)) : '';

    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = [];

    $element['title'] = [
      '#type' => 'radios',
      '#title' => $this->t('Allow text alternative'),
      '#default_value' => $this->getSetting('title'),
      '#options' => [
        DRUPAL_DISABLED => $this->t('Disabled'),
        DRUPAL_OPTIONAL => $this->t('Optional'),
        DRUPAL_REQUIRED => $this->t('Required'),
      ],
    ];

    return $element;
  }

}
