<?php

namespace Drupal\ex_icons;

use Drupal\Core\Template\Attribute;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * A class providing Drupal Twig extensions.
 */
class TwigExtension extends AbstractExtension {

  /**
   * {@inheritdoc}
   */
  public function getFunctions() {
    return [
      new TwigFunction('ex_icon', [$this, 'getIcon']),
    ];
  }

  /**
   * Gets an icon.
   *
   * @param string $icon
   *   The id of the icon.
   * @param array|\Drupal\Core\Template\Attribute $attributes
   *   (optional) Array or Attribute object of SVG attributes.
   * @param string $title
   *   (optional) Accessible title for the SVG icon.
   *
   * @return array
   *   A render array representing an icon.
   */
  public function getIcon($icon, $attributes = [], $title = '') {
    return [
      '#theme' => 'ex_icon',
      '#id' => $icon,
      '#title' => $title,
      '#attributes' => $attributes instanceof Attribute ? $attributes->toArray() : $attributes,
    ];
  }

}
