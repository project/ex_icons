<?php

namespace Drupal\ex_icons\Serialization;

use Drupal\Component\Serialization\SerializationInterface;

/**
 * Provides a SVG sprite sheet serialization implementation.
 *
 * Example file format:
 * @code
 * <svg>
 *   <defs>
 *     <linearGradient id="gradient" x1="0" y1="0" x2="10" y2="10" gradientUnits="userSpaceOnUse">
 *       <stop offset="0" stop-color="#fff"/>
 *       <stop offset="1" stop-color="#000"/>
 *     </linearGradient>
 *   </defs>
 *   <symbol id="icon-1" viewBox="0 0 20 20">
 *     <title>Label of the icon (optional)</title>
 *     <rect width="20" height="20" />
 *   </symbol>
 *   <symbol id="icon-2" viewBox="0 0 20 20">
 *     <title>Label of the icon (optional)</title>
 *     <rect width="20" height="20" fill="url(#gradient)" />
 *   </symbol>
 * </svg>
 * @endcode
 */
class SvgSpriteSheet implements SerializationInterface {

  /**
   * {@inheritdoc}
   */
  public static function encode($data) {
    // Noop.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public static function decode($raw) {
    $data = [
      'icons' => [],
    ];

    $dom = new \DOMDocument();
    $dom->preserveWhiteSpace = FALSE;
    $dom->loadXML($raw);

    foreach ($dom->getElementsByTagName('symbol') as $symbol) {
      // Skip symbols without ID attribute.
      if (!$symbol->hasAttribute('id')) {
        continue;
      }

      $id = $symbol->getAttribute('id');
      $viewbox = explode(' ', $symbol->getAttribute('viewBox'));

      // Catch invalid viewBox attribute values.
      if (count($viewbox) != 4 || count(array_filter($viewbox, 'is_numeric')) != 4) {
        continue;
      }

      // Attempt to extract a text representation from a title element.
      $title = $symbol
        ->getElementsByTagName('title')
        ->item(0);

      $data['icons'][$id] = [
        'width'  => (double) $viewbox[2] - (double) $viewbox[0],
        'height' => (double) $viewbox[3] - (double) $viewbox[1],
        'label'  => $title ? $title->nodeValue : '',
      ];
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public static function getFileExtension() {
    return 'svg';
  }

}
