<?php

/**
 * @file
 * External-use icons API documentation.
 */

/**
 * Alter the list of icon plugin definitions.
 *
 * @param array $definitions
 *   The array of icon plugin definitions, keyed by plugin ID.
 *
 * @see \Drupal\ex_icons\ExIconsManager
 */
function hook_ex_icons_alter(array &$definitions) {
  $definitions['example-icon']['label'] = t('Foobar');
}
