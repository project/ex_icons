<?php

namespace Drupal\Tests\ex_icons\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the functionality of ExIconDefault field formatter.
 *
 * @group ex_icons
 */
class ExIconDefaultFormatterTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ex_icons',
    'ex_icons_test',
    'options',
  ];

  /**
   * Name of fields used in tests, keyed by field type.
   *
   * @var string[]
   */
  protected $fieldNames;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fieldNames = [
      'ex_icon'     => mb_strtolower($this->randomMachineName()),
      'string'      => mb_strtolower($this->randomMachineName()),
      'list_string' => mb_strtolower($this->randomMachineName()),
    ];

    foreach ($this->fieldNames as $type => $field_name) {
      $field_storage = FieldStorageConfig::create([
        'field_name' => $field_name,
        'entity_type' => 'entity_test',
        'type' => $type,
        'settings' => $type == 'list_string' ? ['allowed_values' => ['icon' => 'Icon']] : [],
      ]);
      $field_storage->save();

      FieldConfig::create([
        'field_storage' => $field_storage,
        'bundle' => 'entity_test',
      ])->save();

      $this->container
        ->get('entity_display.repository')
        ->getViewDisplay('entity_test', 'entity_test')
        ->setComponent($field_name, [
          'type' => 'ex_icon_default',
          'settings' => [
            'width'  => 100,
            'height' => '50px',
          ],
        ])
        ->save();
    }
  }

  /**
   * Tests default icon formatter output.
   */
  public function testExIconDefaultFormatter() {
    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display */
    $display = $this->container
      ->get('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test');

    foreach ($this->fieldNames as $type => $field_name) {
      $title = $this->randomMachineName();

      $entity = EntityTest::create(['bundle' => 'entity_test']);
      $entity->$field_name->value = 'icon';
      $entity->$field_name->title = $title;
      $entity->save();

      $build = $display->build($entity);
      $this->render($build);

      $elements = $this->cssSelect('svg');
      $this->assertCount(1, $elements, "SVG element rendered for $type field.");

      $svg = reset($elements);
      $this->assertEquals('100', (string) $svg->attributes()->width, "Width from formatter settings for $type field.");
      $this->assertEquals('50px', (string) $svg->attributes()->height, "Height from formatter settings for $type field.");

      if ($type == 'ex_icon') {
        $elements = $svg->xpath('./preceding-sibling::*');
        $this->assertCount(1, $elements);
        $this->assertEquals($title, (string) reset($elements), "Title value used as the SVG's accessible name for $type field.");
      }

      $this->assertTrue(isset($svg->use), "Has use tag for $type field.");
      $use_attributes = $svg->use->attributes();

      $fragment = parse_url($use_attributes->href, PHP_URL_FRAGMENT);
      $this->assertEquals('icon', $fragment, "SVG uses correct glyph for $type field.");
    }
  }

}
