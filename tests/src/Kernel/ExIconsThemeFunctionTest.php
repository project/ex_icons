<?php

namespace Drupal\Tests\ex_icons\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests icon theme functions.
 *
 * @group ex_icons
 */
class ExIconsThemeFunctionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ex_icons',
    'ex_icons_test',
    'system',
  ];

  /**
   * Tests usage of the icon theme function.
   */
  public function testExIconTheme() {
    $base_build = [
      '#theme' => 'ex_icon',
      '#id' => 'icon',
    ];

    $svg = $this->renderIcon($base_build);
    $this->assertNotNull($svg->use, 'Use child element rendered.');

    $fragment = parse_url($svg->use->attributes()->href, PHP_URL_FRAGMENT);
    $this->assertEquals('icon', $fragment, 'SVG use element references specified icon.');

    $title = $this->randomMachineName();
    $elements = $this->renderIcon($base_build + ['#title' => $title])->xpath('./preceding-sibling::*');
    $this->assertCount(1, $elements, 'Has preceding sibling element.');

    $element = reset($elements);
    $this->assertEquals($title, (string) $element, 'Preceding element content is the SVG title.');
    $this->assertEquals('visually-hidden', (string) $element->attributes()->class, 'SVG title element is visually hidden.');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['width' => 22]]);
    $this->assertEquals('44', (string) $svg->attributes()->height, 'Height calculated from aspect ratio of icon (integer value).');
    $svg = $this->renderIcon($base_build + ['#attributes' => ['width' => '22']]);
    $this->assertEquals('44', (string) $svg->attributes()->height, 'Height calculated from aspect ratio of icon (string value).');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['height' => 22]]);
    $this->assertEquals('11', (string) $svg->attributes()->width, 'Width calculated correctly aspect ratio of icon (integer value).');
    $svg = $this->renderIcon($base_build + ['#attributes' => ['height' => '22']]);
    $this->assertEquals('11', (string) $svg->attributes()->width, 'Width calculated correctly aspect ratio of icon (string value).');

    $svg = $this->renderIcon($base_build + [
      '#attributes' => [
        'width' => 100,
        'height' => 33,
      ],
    ]);
    $this->assertEquals('100', (string) $svg->attributes()->width, 'Dimension attributes as-is when both attributes are declared explicitly.');
    $this->assertEquals('33', (string) $svg->attributes()->height, 'Dimension attributes as-is when both attributes are declared explicitly.');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['width' => '100%']]);
    $this->assertFalse(isset($svg->attributes()['height']), 'No height calculated if width attribute is not a number.');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['height' => '100%']]);
    $this->assertFalse(isset($svg->attributes()['width']), 'No width calculated if height attribute is not a number.');

    $base_build = [
      '#theme' => 'ex_icon',
      '#id' => 'does_not_exist',
    ];

    $this->renderIcon($base_build);
    $elements = $this->cssSelect('svg > use');
    $this->assertCount(1, $elements, 'Non-existent icon is rendered with skeleton output.');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['width' => 20]]);
    $this->assertEquals('20', (string) $svg->attributes()->height, 'Non-existent icon height calculated with 1:1 aspect ratio.');

    $svg = $this->renderIcon($base_build + ['#attributes' => ['height' => 20]]);
    $this->assertEquals('20', (string) $svg->attributes()->width, 'Non-existent icon width calculated with 1:1 aspect ratio.');
  }

  /**
   * Render a an icon render array.
   *
   * @param array $build
   *   The render array.
   *
   * @return \SimpleXMLElement
   *   SVG element from output.
   */
  protected function renderIcon(array $build) {
    $this->setRawContent($this->container->get('renderer')->renderPlain($build));
    $elements = $this->cssSelect('svg');
    return reset($elements);
  }

}
