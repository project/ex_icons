<?php

namespace Drupal\Tests\ex_icons\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\ex_icons\TwigExtension
 * @group ex_icons
 */
class TwigExtensionTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ex_icons',
    'ex_icons_test',
  ];

  /**
   * @covers ::getIcon
   */
  public function testGetIcon() {
    $this->renderTwig('{{ ex_icon("icon") }}');

    $elements = $this->cssSelect('svg');
    $this->assertCount(1, $elements, 'SVG rendered.');

    $svg = reset($elements);
    $fragment = parse_url($svg->use->attributes()->href, PHP_URL_FRAGMENT);
    $this->assertEquals('icon', $fragment, 'Renders specified glyph.');

    $this->renderTwig('{{ ex_icon("icon", { class: ["icon"] }) }}');
    $elements = $this->cssSelect('svg');
    $svg = reset($elements);
    $this->assertEquals('icon', (string) $svg->attributes()->class, 'Renders SVG with specified attributes.');

    $this->renderTwig('{{ ex_icon("icon", create_attribute({ class: ["icon"] })) }}');
    $svg = reset($elements);
    $this->assertEquals('icon', (string) $svg->attributes()->class, 'Can accept Attribute object instead of array for attributes parameter.');
  }

  /**
   * Renders a twig template.
   */
  protected function renderTwig($template) {
    $build = [
      '#type' => 'inline_template',
      '#template' => $template,
    ];

    $this->setRawContent($this->container->get('renderer')->renderRoot($build));
  }

}
