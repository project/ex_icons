<?php

namespace Drupal\Tests\ex_icons\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Tests the functionality of ExIconLinkFormatter field formatter.
 *
 * @group ex_icons
 */
class ExIconLinkFormatterTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'ex_icons',
    'ex_icons_test',
    'link',
  ];

  /**
   * Name of the field used in tests.
   *
   * @var string
   */
  protected $fieldName;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fieldName = mb_strtolower($this->randomMachineName());

    $storage = FieldStorageConfig::create([
      'field_name' => $this->fieldName,
      'entity_type' => 'entity_test',
      'type' => 'link',
    ]);
    $storage->save();

    FieldConfig::create([
      'field_storage' => $storage,
      'bundle' => 'entity_test',
    ])->save();
  }

  /**
   * Tests Link as an Icon formatter output.
   */
  public function testExIconLinkFormatter() {
    $entity_no_title = EntityTest::create(['bundle' => 'entity_test']);
    $entity_no_title->{$this->fieldName}->uri = 'https://example.com/';
    $entity_no_title->{$this->fieldName}->options = [];
    $entity_no_title->save();

    $this->renderEntityWithSettings($entity_no_title, [
      'icon'   => 'icon',
      'width'  => '50',
      'height' => '1em',
    ]);

    $elements = $this->cssSelect('a[href="https://example.com/"] > svg');
    $this->assertCount(1, $elements, 'SVG element rendered within link.');

    $svg = reset($elements);
    $svg_attributes = $svg->attributes();
    $this->assertEquals('50', (string) $svg_attributes->width, 'Width formatter setting as attribute.');
    $this->assertEquals('1em', (string) $svg_attributes->height, 'Height formatter setting as attribute.');

    $elements = $svg->xpath('./preceding-sibling::*[@class="visually-hidden"]');
    $this->assertCount(1, $elements, 'SVG has preceding element as visually hidden.');
    $this->assertEquals($this->fieldName, (string) reset($elements), 'SVG accessible name element uses field label when no title set on link item.');

    $this->assertTrue(isset($svg->use), 'Has use SVG element child.');
    $fragment = parse_url($svg->use->attributes()->href, PHP_URL_FRAGMENT);
    $this->assertEquals('icon', $fragment, 'Use element references glyph from settings.');

    $title = $this->randomMachineName();

    $entity = EntityTest::create(['bundle' => 'entity_test']);
    $entity->{$this->fieldName}->uri = 'https://example.com/';
    $entity->{$this->fieldName}->title = $title;
    $entity->{$this->fieldName}->options = [];
    $entity->save();

    $this->renderEntityWithSettings($entity, ['icon' => 'icon']);

    $elements = $this->cssSelect('a[href="https://example.com/"] > .visually-hidden');
    $this->assertEquals($title, (string) reset($elements), 'SVG has accessible label from link item title.');

    $this->renderEntityWithSettings($entity, [
      'icon'   => 'icon',
      'rel'    => 'nofollow',
      'target' => '_blank',
    ]);

    $elements = $this->cssSelect('a[href="https://example.com/"]');
    $link_attributes = reset($elements)->attributes();
    $this->assertEquals('nofollow', (string) $link_attributes->rel, 'Link rel attribute uses formatter value "nofollow".');
    $this->assertEquals('_blank', (string) $link_attributes->target, 'Link target attribute uses formatter value "_blank".');
  }

  /**
   * Renders an entity_test entity with given link field formatter settings.
   *
   * @param \Drupal\entity_test\Entity\EntityTest $entity
   *   Test entity to render.
   * @param array $settings
   *   Optional. Settings for the Link as Icon formatter.
   */
  protected function renderEntityWithSettings(EntityTest $entity, array $settings = []) {
    $display = $this->container
      ->get('entity_display.repository')
      ->getViewDisplay('entity_test', 'entity_test');

    $display_options = [
      'type' => 'ex_icon_link',
      'settings' => $settings,
    ];

    $display
      ->setComponent($this->fieldName, $display_options)
      ->save();

    $build = $display->build($entity);
    $this->render($build);
  }

}
