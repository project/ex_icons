<?php

namespace Drupal\Tests\ex_icons\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\ex_icons\ExIconsManager
 * @group ex_icons
 */
class ExIconsManagerTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'ex_icons',
    'ex_icons_test',
    'system',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    \Drupal::service('theme_installer')->install(['ex_icons_theme_test']);
  }

  /**
   * Test the icon plugin definitions discovered.
   */
  public function testDiscoveredIcons() {
    $expected = [
      'icon-no-title' => [
        'label' => '',
        'width' => '25',
        'height' => '25',
        'provider' => 'ex_icons_test',
        'id' => 'icon-no-title',
        'class' => 'Drupal\\ex_icons\\ExIcon',
      ],
      'icon' => [
        'label' => 'Icon',
        'width' => '20',
        'height' => '40',
        'provider' => 'ex_icons_test',
        'id' => 'icon',
        'class' => 'Drupal\\ex_icons\\ExIcon',
      ],
      'theme-icon-no-title' => [
        'label' => '',
        'width' => '25',
        'height' => '25',
        'provider' => 'ex_icons_theme_test',
        'id' => 'theme-icon-no-title',
        'class' => 'Drupal\\ex_icons\\ExIcon',
      ],
      'theme-icon' => [
        'label' => 'Icon',
        'width' => '20',
        'height' => '20',
        'provider' => 'ex_icons_theme_test',
        'id' => 'theme-icon',
        'class' => 'Drupal\\ex_icons\\ExIcon',
      ],
    ];

    $definitions = \Drupal::service('ex_icons.manager')->getDefinitions();
    foreach ($expected as $id => $expected_definition) {
      $this->assertEquals($expected_definition['label'], $definitions[$id]['label']);
      $this->assertEquals($expected_definition['width'], $definitions[$id]['width']);
      $this->assertEquals($expected_definition['height'], $definitions[$id]['height']);
      $this->assertEquals($expected_definition['provider'], $definitions[$id]['provider']);
      $this->assertEquals($expected_definition['id'], $definitions[$id]['id']);
      $this->assertEquals($expected_definition['class'], $definitions[$id]['class']);
      $this->assertMatchesRegularExpression('`^/modules/.*ex_icons/tests/(themes|modules)/.+/dist/icons.svg\?[0-9a-f]+#`', $definitions[$id]['url']);
    }
  }

  /**
   * @covers ::getInstance
   */
  public function testGetInstance() {
    $manager = \Drupal::service('ex_icons.manager');

    // Test that an existing ID returns the instance.
    $instance = $manager->getInstance(['id' => 'icon']);
    $this->assertEquals('icon', $instance->getPluginId());

    // Test that an non-existing ID returns the fall-back null instance.
    $instance = $manager->getInstance(['id' => 'does_not_exist']);
    $this->assertEquals('ex_icon_null', $instance->getPluginId());
  }

  /**
   * @covers ::getIconOptions
   */
  public function testGetIconOptions() {
    $manager = \Drupal::service('ex_icons.manager');

    $expected = [
      'icon-no-title' => 'icon-no-title',
      'icon' => 'Icon',
      'theme-icon-no-title' => 'theme-icon-no-title',
      'theme-icon' => 'Icon',
    ];
    $this->assertEqualsCanonicalizing(array_keys($expected), array_keys($manager->getIconOptions()));
    $this->assertEqualsCanonicalizing($expected, $manager->getIconOptions());
  }

}
