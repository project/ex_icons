<?php

namespace Drupal\Tests\ex_icons\Unit;

use Drupal\ex_icons\Serialization\SvgSpriteSheet;
use Drupal\Tests\UnitTestCase;

/**
 * @coversDefaultClass \Drupal\ex_icons\Serialization\SvgSpriteSheet
 * @group ex_icons
 */
class SvgSpriteSheetTest extends UnitTestCase {

  /**
   * @covers ::decode
   */
  public function testDecoding() {
    $content = file_get_contents(__DIR__ . '/../../fixtures/sprites.svg');
    $result = SvgSpriteSheet::decode($content);

    $this->assertEqualsCanonicalizing(
      [
        'icon-no-title' => [
          'width'  => 25,
          'height' => 25,
          'label'  => '',
        ],
        'icon' => [
          'width'  => 20,
          'height' => 60,
          'label'  => 'Icon',
        ],
        'offset-viewbox' => [
          'width'  => 100,
          'height' => 50,
          'label'  => '',
        ],
      ],
      $result['icons']
    );

    $content = file_get_contents(__DIR__ . '/../../fixtures/icons-invalid.svg');
    $result  = SvgSpriteSheet::decode($content);
    $this->assertCount(0, $result['icons'], 'No invalid symbols registered.');
  }

}
