<?php

namespace Drupal\Tests\ex_icons\Unit;

use Drupal\Component\FileCache\FileCacheFactory;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\File\FileUrlGeneratorInterface;
use Drupal\ex_icons\Discovery\SvgSymbolDiscovery;
use Drupal\Tests\UnitTestCase;
use org\bovigo\vfs\vfsStream;
use org\bovigo\vfs\vfsStreamDirectory;
use org\bovigo\vfs\vfsStreamWrapper;

/**
 * @coversDefaultClass \Drupal\ex_icons\Discovery\SvgSymbolDiscovery
 * @group ex_icons
 */
class SvgSymbolDiscoveryTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    FileCacheFactory::setConfiguration([FileCacheFactory::DISABLE_CACHE => TRUE]);
  }

  /**
   * Tests the SVG symbol discovery.
   */
  public function testDiscovery() {
    vfsStreamWrapper::register();
    $root = new vfsStreamDirectory('modules');
    vfsStreamWrapper::setRoot($root);
    $url = vfsStream::url('modules');

    mkdir("$url/test_1/test", 0777, TRUE);
    copy(__DIR__ . '/../../fixtures/sprites.svg', "$url/test_1/test/sprites.svg");

    // Directory with no icon sheet.
    mkdir("$url/test_2");

    // Set up the directories to search.
    $directories = [
      'test_1' => "$url/test_1",
      'test_2' => "$url/test_2",
    ];

    $file_url_generator = $this->createMock(FileUrlGeneratorInterface::class);
    $file_url_generator
      ->method('generateString')
      ->willReturnCallback(function ($path) {
        return "transformed:$path";
      });
    $container = new ContainerBuilder();
    $container->set('file_url_generator', $file_url_generator);
    $container->setParameter('app.root', $url);
    \Drupal::setContainer($container);

    $discovery = new SvgSymbolDiscovery('test/sprites', $directories);
    $data = $discovery->findAll();

    $this->assertEquals(1, count($data));
    $this->assertArrayHasKey('test_1', $data);

    $this->assertArrayHasKey('icons', $data['test_1']);
    $this->assertArrayHasKey('icon', $data['test_1']['icons']);
    $this->assertArrayHasKey('icon-no-title', $data['test_1']['icons']);

    $this->assertArrayHasKey('base_url', $data['test_1']);
    $this->assertStringStartsWith('transformed:test_1/test/sprites.svg?', $data['test_1']['base_url']);

    copy(__DIR__ . '/../../fixtures/sprites-updated.svg', "$url/test_1/test/sprites.svg");
    $previous_hash = parse_url($data['test_1']['base_url'], PHP_URL_QUERY);
    $new_hash = parse_url($discovery->findAll()['test_1']['base_url'], PHP_URL_QUERY);
    $this->assertFalse($previous_hash === $new_hash, 'Hash is updated when content is changed.');
  }

}
