<?php

namespace Drupal\Tests\ex_icons\Functional;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests the Ex Icons selection field widget.
 *
 * @group ex_icons
 */
class ExIconsSelectWidgetTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'entity_test',
    'ex_icons',
    'ex_icons_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * A field instance used in tests.
   *
   * @var \Drupal\field\Entity\FieldConfig
   */
  protected $field;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->drupalLogin($this->drupalCreateUser([
      'view test entity',
      'administer entity_test content',
    ]));

    $field_name = mb_strtolower($this->randomMachineName());

    $fieldStorage = FieldStorageConfig::create([
      'field_name' => $field_name,
      'entity_type' => 'entity_test',
      'type' => 'ex_icon',
    ]);
    $fieldStorage->save();

    $this->field = FieldConfig::create([
      'field_storage' => $fieldStorage,
      'bundle' => 'entity_test',
      'label' => 'Category Icon',
    ]);
    $this->field->save();

    \Drupal::service('entity_display.repository')
      ->getFormDisplay('entity_test', 'entity_test')
      ->setComponent($field_name, ['type' => 'ex_icon_select'])
      ->save();
  }

  /**
   * Tests the text setting of an ex icon field with the widget.
   */
  public function testIconTitle() {
    $assert_session = $this->assertSession();
    $field_name = $this->field->getName();

    foreach ([DRUPAL_DISABLED, DRUPAL_REQUIRED, DRUPAL_OPTIONAL] as $title_setting) {
      // Update the text field setting.
      $this->field->setSetting('title', $title_setting);
      $this->field->save();

      $this->drupalGet('entity_test/add');

      // Field label is shown.
      $assert_session->pageTextContains('Category Icon');
      // Icon value exists.
      $assert_session->fieldExists("{$field_name}[0][value]");

      if ($title_setting === DRUPAL_DISABLED) {
        // Text field does not exist if disabled.
        $assert_session->fieldNotExists("{$field_name}[0][title]");

        $elements   = $this->cssSelect('fieldset[data-drupal-selector="edit-' . $field_name . '-0-value"] > legend');
        $icon_label = reset($elements);
        $this->assertStringContainsString('Category Icon', $icon_label->getText(), 'Single field value with no title field uses field label as icon selector label.');
      }
      else {
        // Text field exists.
        $assert_session->fieldExists("{$field_name}[0][title]");
        $title = $this->randomMachineName();

        if ($title_setting === DRUPAL_OPTIONAL) {
          // Verify that the icon is required, if the text is non-empty.
          $edit = [
            "{$field_name}[0][title]" => $title,
          ];
          $this->submitForm($edit, 'Save');
          $assert_session->pageTextContains(
            'The Icon field is required when the Text alternative field is specified.'
          );

          $edit = ["{$field_name}[0][value]" => 'icon'];
          $this->submitForm($edit, 'Save');
        }
        if ($title_setting === DRUPAL_REQUIRED) {
          // Verify that the text is required if an icon is selected.
          $edit = ["{$field_name}[0][value]" => 'icon'];
          $this->submitForm($edit, 'Save');
          $assert_session->pageTextContains(
            'Text alternative field is required if Icon has a selection.'
          );

          // Verify that the text is not required, if the URL is empty.
          $edit = ["{$field_name}[0][value]" => ''];
          $this->submitForm($edit, 'Save');
          $assert_session->pageTextNotContains('Text alternative field is required.');

          // Verify that an icon selection and text meets requirements.
          $this->drupalGet('entity_test/add');
          $edit = [
            "{$field_name}[0][value]" => 'icon',
            "{$field_name}[0][title]" => $title,
          ];
          $this->submitForm($edit, 'Save');
          $assert_session->pageTextNotContains('Text alternative field is required.');
        }

        preg_match('|entity_test/manage/(\d+)|', $this->getUrl(), $match);
        $id = $match[1];
        $this->assertSession()->pageTextContains("entity_test $id has been created.");

        $entity = EntityTest::load($id);
        $this->assertEquals($title, $entity->{$field_name}->title);

        $this->drupalGet($entity->toUrl('edit-form'));
        $this->assertSession()->fieldValueEquals("{$field_name}[0][title]", $title);
      }
    }
  }

}
